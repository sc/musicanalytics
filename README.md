Project musicanalitics

So far:
* _rawAnalyse.R_ : script R to test the function out of shiny (ie in command line just to darw the basics grpahs and check) 
* _generalFunctions.R_: some first exemple of functions that can be used in rawAnalyse.R or shinyServer.R
* _shinyServer.R_: a first test of shiny server

It's possible to try shinyServer.R directly by doing:
```bash
Rscript shinyServer.R 
```

the output should be something like:

```
Loading required package: methods

Listening on http://127.0.0.1:3457
```

and you can open the link given (http://127.0.0.1:3457 but don't forget to change)

Be aware that this could take a few minutes as it has to load all the file each of one is baout 150MO

